/**
   11.2019, 3rd-Landdetector von Julian Franz und Michael Wolkstein
   Produktionsversion 1.0
*/


// c_library_v2 künftig über git submodule

#include "/home/wolke/gitlocal/wolkstein/c_library_v2/common/mavlink.h"
#include "Vars.h"
#include "erdgh.h"
#include <MadgwickAHRS.h>

#define useTfMiniPlus //uncomment to use tfmini plus
#ifdef useTfMiniPlus
#include <TFMPlus.h>  // Include TFMini Plus Library >= v1.3.4
#endif

//#define useTfMini //uncomment to use tfmini plus
#ifdef useTfMini
#include "TFMini.h"
#endif

#include <EEPROM.h>
#include "Config.h"

#define useFireFilterDeltaAccX
#ifdef useFireFilterDeltaAccX
#include <FIR.h>
FIR<int, 8> fir;
#endif

#ifdef useTfMiniPlus
TFMPlus tfmP;         // Create a TFMini Plus object
#endif

#ifdef useTfMini
TFMini tfmini;
#endif

Madgwick filterAHRS;

#define rangeFinderPlumbAndEuler
//#define debugRangeFinderPlumbAndEuler
#define scaleEnable
//#define debugSerialTwo
//#define debugStateMAchine
//#define debugScale
//#define armDebug
//#define modeDebug
//#define paramDeburg
//#define debugZacc
//#define debugStreams
//#define rngfndDebug
//#define debugHeading
//#define debugVibrationZ
//#define setParamEnable
//#define changeModeEnable
//#define armingMotorsEnable //arming (true); will run motors. only activate if you know what you are doing!
//#define stableTemperaturCalibration//Copter will switch to Land to prevent start. It will reboot after Temperatur is stable
#define next_interval_MAVLink  1000  // next interval to count
//#define use_rc_over_mavlink_input


#ifdef scaleEnable
#include "HX711-multi.h"
// HX711 circuit wiring
#define CLK 14      // clock pin to the load cell amp
#define DOUT1 15    // data pin to the first lca
#define DOUT2 16    // data pin to the second lca
//#define DOUT3 17    // data pin to the second lca
//#define DOUT4 18    // data pin to the second lca

//#define DOUT3 A3    // data pin to the third lca
//#define TARE_TIMEOUT_SECONDS 9
byte DOUTS[2] = {DOUT1, DOUT2};
//byte DOUTS[4] = {DOUT1, DOUT2, DOUT3,DOUT4};
//byte DOUTS[1] = {DOUT1};
#define CHANNEL_COUNT  sizeof(DOUTS)/sizeof(byte)
int32_t results[CHANNEL_COUNT];
HX711MULTI scales(CHANNEL_COUNT, DOUTS, CLK);
#endif

#ifdef rangeFinderPlumbAndEuler
ERDGH myLandingGear;
#endif

// Serial input
String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete



void setup() {
  // Pixhawk hangs on boot ?? Serial problem ??


  //Fir Filter
#ifdef useFireFilterDeltaAccX
  int coef[8] = { 1, 1, 1, 1, 1, 1, 1, 1};
  // Set the coefficients
  fir.setFilterCoeffs(coef);
#endif

  pinMode(13, OUTPUT);
  delay(3000);//warmup to avoid temperature jumps at the beginning
#ifdef scaleEnable
  scales.tare(10, 100000);
#endif
  Serial.begin(57600);           //Baud rate for Debug
  delay(1000);

  Serial1.begin(500000);//115200);// Baud rate for MavLink
  delay(1000);
#ifdef debugSerialTwo
  Serial2.begin(250000); // WlanDebug to Serial two
  delay(1000);
#endif

#ifdef useTfMiniPlus
  Serial3.begin( 115200);  // Initialize TFMPLus device serial port.
  delay(1000);               // Give port time to initalize
  tfmP.begin( &Serial3);   // Initialize device library object and...
  delay(500);
  frameRate(100);
  //if(tfmP.sendCommand( I2C_FORMAT_MM, 0)) Serial.println("TfMini returns in mm");

#endif


#ifdef useTfMini
  Serial3.begin(TFMINI_BAUDRATE);
  delay(500);
  tfmini.begin(&Serial3);
  delay(500);
  Serial.println("init rangefinder");
#endif

  //EEPROM Stuff
  // We try if EEPROM is ready
  // we have 2048 byts of eeprom
  // Settings use 124byte + etest 2byte one row of error use 40byte
  uint16_t etest = 0;
  EEPROM.get(0, etest);
  Serial.println(etest);
  if (etest == 1324) {
    Serial.println("Read Eeprom");
    EEPROM.get(sizeof(uint16_t), currentConfig);
    delay(100);

    /* currentConfig.FREEONE = -100.0f;
      currentConfig.FREETWO = -100.0f;
      currentConfig.FREETHREE = -100.0f;
      currentConfig.FREEFOUR = -100.0f;*/

    // eeprom debug
    //Serial.printf("Size of Structs, %d %d %d\n",sizeof(uint16_t),sizeof(currentConfig),sizeof(myErrors));
    //compute _nextLogAddress
    //clearEepromErrorLog();
    int thisIndex = 1;
    for (int i = 150; i < 2000; i = i + 50) {
      EEPROM.get(i, myErrorsTest);
      //Serial.printf("Index: %d Address: %d: Valid: %d\n",thisIndex, i, myErrorsTest.VALID);
      if (myErrorsTest.VALID == 112) _nextLogID = myErrorsTest.ID + 1;
      if (myErrorsTest.VALID == 0 ) { //this hold no data
        _nextLogAddress = i;
        if (myErrorsTest.ID >= 1) _nextLogID = myErrorsTest.ID + 37;
        break;
      }
      thisIndex++;
    }
    //Serial.printf("Nachste Log Adresse: %d: _nextLogID: %d\n",_nextLogAddress,_nextLogID);


  }
  else
  {
    Serial.println("Init Eeprom");
    EEPROM.put(0, 1324);
    // get dafault values from config
    currentConfig.MINGEARTOGROUNDDISTANCE_Z = minGearToGroundDistance;
    currentConfig.MINACTIVATIONALT_Z = minActivationAlt;
    currentConfig.DMSMINWEIGHT = minWeight;
    currentConfig.STOREACCPEAKTIME = storeAccPeakTime;
    currentConfig.MINACC = minAcc;
    EEPROM.put(sizeof(uint16_t), currentConfig);
    clearEepromErrorLog();
  }


#ifdef rangeFinderPlumbAndEuler
  filterAHRS.begin(240); // 6 x faster than 40Hz mavlink raw imu
  //myLandingGear.setDimensions(28.0f, 15.0f, 0.0f, 27.0f, 25.5f, 16.5f, -25.5f, 16.5f, -25.5f, -16.5f, 25.5f, -16.5f);//pappe

  // coords +X point to front, +Y point to right, +Z point to top
  myLandingGear.setDimensions(currentConfig.IMU_Z,
                              currentConfig.RAN_X, // 10.4
                              currentConfig.RAN_Y,
                              currentConfig.RAN_Z,// 38
                              currentConfig.PONE_X,
                              currentConfig.PONE_Y,
                              currentConfig.PTWO_X,
                              currentConfig.PTWO_Y,
                              currentConfig.PTHREE_X,
                              currentConfig.PTHREE_Y,
                              currentConfig.PFOUR_X,
                              currentConfig.PFOUR_Y);
#endif
  Serial.println("Ready");

}



void loop() {
  //loopMeasure=micros();

  if ((millis() - 10000) > ultraslowLoop) {
    ultraslowLoop = millis();

    Mav_Request_Data();
#ifdef debugStreams
    Serial.println ("StreamsRequested");
#endif
  }

  if ((millis() - 1000) > slowLoop) {
    slowLoop = millis();
    //if(__SendHB) Heartbeat();
    if (currentConfig.FREEFIVE == 1.0f) Heartbeat();
  }


  if ((millis() - 100) > midLoop) {
    midLoop = millis();
    //changeMode(4);
    //setParam("SERIAL2_BAUD",57);
#ifdef scaleEnable
    processScale();
#endif

#ifdef useTfMiniPlus
    if (currentConfig.FREESIX == 1.0f) mavlink_test_distance_sensor(tfDist, 1);
#endif
    //Serial.println("Teste Serial output timout 100ms");
  }

#ifdef rangeFinderPlumbAndEuler
  if ((millis() - 30) > plumbLoop) {
    plumbLoop = millis();
#ifdef useTfMini
    float localdistanceSensor = tfmini.getDistance();
    if (localdistanceSensor > 150.0f) { // ist der alze wert von zb. 70 auf 151 gesprungen würde der olte wert auf 70cm hängen bleiben. das verhindert im anscgluß eine hohe sinkrate erkennung. unerwünscht!
      if (__olddistanceSensor <= 150.0) __olddistanceSensor = __olddistanceSensor + 0.5;
      __distanceSensor = __olddistanceSensor;
    } else
    {
      __distanceSensor = localdistanceSensor;
      __olddistanceSensor = __distanceSensor;
    }
#endif

#ifdef useTfMiniPlus
    tfmP.getData(tfDist, tfFlux, tfTemp);
    if (tfDist != 0) tfDist_used = tfDist;

    float localdistanceSensor = float(tfDist_used);
    //Serial.printf("hohe: %d cm,hoehe_used: %dcm, lux: %d lux,temp: %d grad\n",tfDist,tfDist_used,tfFlux,tfTemp);
    if (localdistanceSensor > 150.0f) { // ist der alze wert von zb. 70 auf 151 gesprungen würde der olte wert auf 70cm hängen bleiben. das verhindert im anscgluß eine hohe sinkrate erkennung. unerwünscht!
      if (__olddistanceSensor <= 150.0) __olddistanceSensor = __olddistanceSensor + 0.5;
      __distanceSensor = __olddistanceSensor;
    } else
    {
      __distanceSensor = localdistanceSensor;
      __olddistanceSensor = __distanceSensor;
    }
#endif

    //Serial.printf("%.2f, %.2f\n",__distanceSensor, localdistanceSensor);

    computePlunb();
    computeVirtualLandingGear();
  }
#endif

  if ((millis() - 4) > fastLoop) {
    fastLoop = millis();
    // Compute Plumb and Rangefinder at 40Hz
#ifdef rangeFinderPlumbAndEuler
    computeAHRS(); // compute Attitude Pitch and Roll
#endif

  }

  if ((millis() - 10) > debugLoop) {
    debugLoop = millis();
    // Debug On Wlan
#ifdef debugSerialTwo
    debugWlan();
#endif
    if ( __debugSerialTwoToConsoleOne ) {
      Serial.printf("%d, %.2f, %d, %.2f, %d, %d, %d, %d, %d, %.4f, %.4f, %d, %.2f, %.2f, %d, %d ,%d ,%d, %d, %d\n"
                    , millis() //__systemTimeMs
                    , float(__mavlinkRawAltInt) / 10.0f /* -40.0*/ // mm /10 = cm
                    , __mavlinkRawGpsAlt
                    , __lowGearPlumb
                    , __lowGearCorner
                    , weight3
                    , weight4
                    , weight
                    , weight2
                    , __attitudePitchRad
                    , __attitudeRollRad
                    , zacc
                    , __distanceSensor
                    , __sinkRateAverage
                    , Rc10
                    , __MAIN_STATE
                    , __EXT_STATE_INTERRUPT
                    , __stateError
                    , accPeak
                    , _bam );
    }
    //if(accPeak || _bam) Serial.printf("ACCZ Peak: %d\t ACCZ Delta: %d\n--------------\n",accPeak,_bam );
  }

  storeAccPeak();
  simpleFsmComputeState();
  // ChecCONSOLE_DATA_ONk reception buffer
  comm_receive();
  //loopTime =micros()-loopMeasure;
  checkSerialInput();

}


//tfmini plus helper
void frameRate( uint16_t rate)
{
 #ifdef useTfMiniPlus
  Serial.printf( "Lidar frame rate ");
  if ( tfmP.sendCommand( SET_FRAME_RATE, rate))
  {
    Serial.printf( "set to %2uHz.\r\n", rate);
  }
  else
  {
    Serial.printf( "command failed.");
    tfmP.printStatus( false);
  }
  #endif
}
