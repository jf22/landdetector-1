/**
 * Class ERD_LANDINGGEAR
 * 3rd-element landegestell
 * 
 * 
 * Class definition of Wolke Landegestell
 * *******************************************
 * Virual landing gear rotation in euler rotationmatrix.
 * 
 * depend to BasicLinearAlgebra & Geometry both from Tom Steward
 *
 */

#include "Arduino.h"
#include "erdgh.h"

ERDGH::ERDGH()
{
   setDimensions(40.0f, // Copter rotation centerpoint Z from ground level
                 18.0f, 0.0f, 36.0f, // Rangefinder offset from copter rotation center(imu)
                 32.0f, 32.0f, //Landing gear corner front right
                 -32.0f, 32.0f, //Landing gear corner rear right
                 -32.0f, -32.0f, //Landing gear corner rear left
                 32.0f, -32.0f //Landing gear corner front left
   );
   
  Rot.FromEulerAngles(0.0, 0.0, 0.0); // init rotation wit zeros
  PcC = Rot * p_CRCP;
  PgFR = Rot * p_GFR;
  PgRR = Rot * p_GRR;
  PgRL = Rot * p_GRL;
  PgFL = Rot * p_GFL;

  lowestGearLevel = rotationMatrix[5];
  lowestCorner = 0;
}

void ERDGH::setDimensions(float rMPZ, // Copter rotation centerpoint Z from ground level
                          float rFPX, float rFPY, float rFPZ, // Rangefinder offset from copter rotation center(imu)
                          float rGFRX, float rGFRY, //Landing gear corner front right
                          float rGRRX, float rGRRY, //Landing gear corner rear right
                          float rGRLX, float rGRLY, //Landing gear corner rear left
                          float rGFLX, float rGFLY //Landing gear corner front left
                          )
{
     
    rotationMatrix[2] = rMPZ;

    rotationMatrix[3] = rFPX;
    rotationMatrix[4] = rFPY;
    rotationMatrix[5] = rFPZ;
    
    rotationMatrix[6] = rGFRX;
    rotationMatrix[7] = rGFRY;
    
    rotationMatrix[9] = rGRRX;
    rotationMatrix[10] = rGRRY;
    
    rotationMatrix[12] = rGRLX;
    rotationMatrix[13] = rGRLY;

    rotationMatrix[15] = rGFLX;
    rotationMatrix[16] = rGFLY;

    p_CRCP.X() = rotationMatrix[0] - rFPX;
    p_CRCP.Y() = rotationMatrix[1] - rFPY;
    p_CRCP.Z() = rMPZ - rFPZ;

    //Rangefinder trickreich im rotationsmittelpunkt.
    p_RF.X() = 0.0;
    p_RF.Y() = 0.0;
    p_RF.Z() = 0.0;

    //Landegestell vorne links ist auf bodenhöhe 0 und 30cm nach vorne und 29cm nach links

    p_GFR.X() = rotationMatrix[6] - rFPX;
    p_GFR.Y() = rotationMatrix[7] - rFPY;
    p_GFR.Z() = rotationMatrix[8] - rFPZ;

    p_GRR.X() = rotationMatrix[9] - rFPX;
    p_GRR.Y() = rotationMatrix[10] - rFPY;
    p_GRR.Z() = rotationMatrix[11] - rFPZ;

    p_GRL.X() = rotationMatrix[12] - rFPX;
    p_GRL.Y() = rotationMatrix[13] - rFPY;
    p_GRL.Z() = rotationMatrix[14] - rFPZ;

    p_GFL.X() = rotationMatrix[15] - rFPX;
    p_GFL.Y() = rotationMatrix[16] - rFPY;
    p_GFL.Z() = rotationMatrix[17] - rFPZ;

}

void ERDGH::solveEulerRotation(float xrad, float yrad, float zrad){

  Rot.FromEulerAngles(xrad, yrad,zrad ); // init rotation wit zeros
  PcC = Rot * p_CRCP;
  PgFR = Rot * p_GFR;
  PgRR = Rot * p_GRR;
  PgRL = Rot * p_GRL;
  PgFL = Rot * p_GFL;

  lowestGearLevel = rotationMatrix[5];
  lowestCorner = 0;

  if(PgFR.Z() <= lowestGearLevel){
    lowestCorner = F_R;
    lowestGearLevel = PgFR.Z();
  }
  if(PgRR.Z() <= lowestGearLevel){
    lowestCorner = R_R;
    lowestGearLevel = PgRR.Z();
  }
  if(PgRL.Z() <= lowestGearLevel){
    lowestCorner = R_L;
    lowestGearLevel = PgRL.Z();
  }
  if(PgFL.Z() <= lowestGearLevel){
    lowestCorner = F_L;
    lowestGearLevel  = PgFL.Z();
  }
  return;
}

float ERDGH::getLowestGearLevel(){
    return lowestGearLevel;
}

int ERDGH::getLowestCorner(){
    return lowestCorner;
}
