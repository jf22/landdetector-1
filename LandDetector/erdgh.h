/**
 * Class ERD_LANDINGGEAR
 * 3rd-element landegestell
 * 
 * 
 * Class definition of Wolke Landegestell
 * *******************************************
 * Virual landing gear rotation in euler rotationmatrix.
 *
 */

#ifndef _ERDLG_H_
#define _ERDLG_H_

#include "Arduino.h"
#include <Geometry.h>
#include <BasicLinearAlgebra.h>

class ERDGH {

public:
    
    /**
     *  brief ERDGH class constructor
     * Example ERDGH myErdgh();
    */
    ERDGH();

    /**
     * set all poits in 3d Matrix
     * that need 12 arguments in cm
     * x point to front and rear, negativ values point to rear
     * y point to left and right, negativ values pofloat rMPX, float rMPY, float rMPZint to left
     * z point to top left, negativ values point downwards 
     * 1 Rotation Center of Copter (IMU) x and y is 0 Z value is needed. Start abs. from groundlevel for example  Z +40)
     * 2-4 Rangefinder Coordinates abs. from ground and (rotationcenter x y)
     * 5-6 Landing gear corner front right from rotationcenter x y
     * 7-8 Landing gear corner rear right from rotationcenter x y
     * 9-10 Landing gear corner rear left from rotationcenter x y
     * 11-12 Landing gear corner front left from rotationcenter x y
     */
    void setDimensions(float rMPZ, // Copter rotation centerpoint Z from ground level
                  float rFPX, float rFPY, float rFPZ, // Rangefinder offset from copter rotation center(imu)
                  float rGFRX, float rGFRY, //Landing gear corner front right
                  float rGRRX, float rGRRY, //Landing gear corner rear right
                  float rGRLX, float rGRLY, //Landing gear corner rear left
                  float rGFLX, float rGFLY //Landing gear corner front left
                  );

    void solveEulerRotation(float xrad, float yrad, float zrad); // call to compute new values

    float getLowestGearLevel(); // return lowest gear level

    int getLowestCorner(); // return nuber of lowest corner 1 = Front Right, 2 = Rear Right, 3 = Rear Left, 4 = Front Left
    
    
    
private:
    

    float rotationMatrix[19] =
    {
      0.0f,            // Copter Rotation Center X
      0.0f,            // Copter Rotation Center Y
      40.0f,           // Copter Rotation Center Z

      18.0f,           // Rangefinder X offset from C R C
      0.0f,            // Rangefinder Y offset from C R C
      36.0f,           // Rangefinder Z offset from Groundlevel
      
      32.0f,           // Landing gear Corner Front Right X from C R C
      32.0f,           // Landing gear Corner Front Right Y from C R C 
      0.0,             // Landing gear Corner Front Right Z from C R C
      
      -32.0f,          // Landing gear Corner Rear Right X from C R C
      32.0f,           // Landing gear Corner Rear Right Y from C R C 
      0.0,             // Landing gear Corner Rear Right Z from C R C

      -32.0f,           // Landing gear Corner Rear Left X from C R C
      -32.0f,           // Landing gear Corner Rear Left Y from C R C 
      0.0,              // Landing gear Corner Rear Left Z from C R C

      32.0f,           // Landing gear Corner Front Left X from C R C
      -32.0f,          // Landing gear Corner Front Left Y from C R C 
      0.0,             // Landing gear Corner Front Left Z from C R C 
    };
    
    enum Corners{
      non,
      F_R,
      R_R,
      R_L,
      F_L
    };


    float lowestGearLevel;
    int lowestCorner;
    // Point represents a coordinate in 3D space
    Point p_CRCP, p_RF, p_GFR, p_GRR, p_GRL, p_GFL; // Geometry Points as Vector
    Point PcC, PgFR, PgRR, PgRL, PgFL;
    Rotation Rot;
    
    
};
#endif /* _LSCM_H_ */
 
